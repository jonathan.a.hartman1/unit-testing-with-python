# Define the winning conditions
win_conditions = {
    'rock': {'scissors'},
    'paper': {'rock'},
    'scissors': {'paper'}
}

def get_player_choice(player: str) -> str:
    """
    Get the player's choice of rock, paper, or scissors.

    Will continue to prompt the player until a valid choice is entered.

    Args:
        player (str): The player's name.

    Returns:
        str: The player's choice.
    """
    while True:
        try:
            choice = input(f"{player}: ").lower()
            if choice not in ['rock', 'paper', 'scissors']:
                raise ValueError("Invalid choice. Please choose rock, paper, or scissors.")
            return choice
        except ValueError as e:
            print(e)

# Get the player choices
player1_choice = get_player_choice("Player 1")
player2_choice = get_player_choice("Player 2")

# Determine the winner
print("--------------------")

if player1_choice == player2_choice:
    print("It's a tie!")
elif player2_choice in win_conditions[player1_choice]:
    print("Player 1 wins!")
else:
    print("Player 2 wins!")